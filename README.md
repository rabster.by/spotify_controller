# Spotify Controller

This is the code to go along with a youtube video you can watch [here](https://youtu.be/SWiPIBWvgIU).

This code is for a device that will control spotify using their web api.

## Required libraries (available from Sketch>Include Library>Manage Libraries In the arduino IDE)
TFT_eSPI

TJpg_Decoder

ArduinoJson

## Getting the display working
Open the arduino IDE and then go to file>examples>TFT_eSPI>TFT_graphicstest_small . Once that is open go to Sketch>Show Sketch Folder (or press ctrl+k) . This should take you to a file with a path like <Something>/Arduino/libraries/TFT_eSPI/examples/<something> . You need to navigate up to the TFT_eSPI folder and find a file called User_Setup_Select.h. Open this file with a text editor and inside you should see a whole bunch of commented out lines that say #include <Something> make sure they are all commented out except for the one that says #include <User_Setups/Setup2_ST7735.h> . This should hopefully be all you need to do but if things still are not working you can go into /Arduino/libraries/TFT_eSP/User_Setups/Setup2_ST7735.h and it will have the pinouts and other information inside of it. In Setup2_ST7735.h you may need to try replacing line 11 (#define ST7735_REDTAB) with #define ST7735_GREENTAB . The "tab" being referred to is the tab color of the screen protector that came on the device. The full list of what that can be replaced with are
// #define ST7735_INITB
//  #define ST7735_GREENTAB
// #define ST7735_GREENTAB2
// #define ST7735_GREENTAB3
// #define ST7735_GREENTAB128    // For 128 x 128 display
// #define ST7735_GREENTAB160x80 // For 160 x 80 display (BGR, inverted, 26 offset)
// #define ST7735_ROBOTLCD       // For some RobotLCD arduino shields (128x160, BGR, https://docs.arduino.cc/retired/getting-started-guides/TFT)
// #define ST7735_REDTAB
// #define ST7735_BLACKTAB
// #define ST7735_REDTAB160x80   // For 160 x 80 display with 24 pixel offset

## Set up steps

1. Go to https://developer.spotify.com/documentation/web-api and follow the instructions there to create an app.
2. In the app you have created there is a client_ID and client_Secret, copy these values into the spotify_buddy.ino at lines 62 and 63.
3. While you are editing that code also change the WIFI_SSID and PASSWORD on lines 58 and 59 to your local wifi name and password.
4. Upload the code
5. Once thats done you can start the device and it will display an IP on the screen. Write this down.
6. In the code change the YOUR_ESP_IP on line 64 to the ip you wrote down.
7. Go back to the spotify set up and copy line 64 of the code (""http://YOUR_ESP_IP/callback") into the Redirect URIs section of your app setting page.
8. Upload the code again and when the device starts go to the ip in a browser and follow the instructions. Everything should now work!

